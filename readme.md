# Process status logger 

1. Memory usage
2. CPU load
3. Start status

### Usage
```javascript
memoryUsage({ 
    saveTimer: 1000 * 60,
    saveTransportFunc: (...arg) => redisGS.set(...arg),
    savePath: 'Tickers_memory',
    logExpire: 60 * 10,
    otherParams: {
        processName: 'main',
        appName: 'tickers',
    },
});
```