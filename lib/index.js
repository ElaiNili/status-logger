'use strict';

// ------------------- конвертация памяти ------------------
const bytesToMb = bytes => Math.round(bytes / 1000, 2) / 1000;

module.exports = function memoryUsage(args) {
    
    const {
        processName = 'main',
        appName = '',
        measurTime = '',
        pid = process.pid,
    } = args;

    const usage = process.memoryUsage();

    const resultData = {
        ...args,
        processName, appName, 
        pid, measurTime,
        rss: bytesToMb(usage.rss), // process resident set size
        heapTotal: bytesToMb(usage.heapTotal), // v8 heap allocated
        heapUsed: bytesToMb(usage.heapUsed), // v8 heap used
        external: bytesToMb(usage.external), // c++ allocated
        stack: bytesToMb(usage.rss - usage.heapTotal), // stack
    };

    return resultData;
};